import type { LocaleText } from '~/models/locale-text.model';

export type IEncounterMethod = {
	id: number;
	name: string;
	order: number;
	names: LocaleText<'name'>[];
};
