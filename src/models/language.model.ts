import type { LocaleText } from '~/models/locale-text.model';

export type ILanguage = {
	id: number;
	name: string;
	isOfficial: boolean;
	names: LocaleText<'name'>[];
};
