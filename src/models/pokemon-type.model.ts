import type { LocaleText } from '~/models/locale-text.model';

export type IPokemonType = {
	id: number;
	name: string;
	names: LocaleText<'name'>[];
};
