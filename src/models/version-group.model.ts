import type { IGeneration } from '~/models/generation.model';
import type { IPokedex } from '~/models/pokedex.model';
import type { IRegion } from '~/models/region.model';

export type IVersionGroup = {
	id: number;
	name: string;
	order: number;
	generation: IGeneration;
	pokedexes: IPokedex[];
	regions: IRegion[];
};
