import type { IPokemonType } from '~/models/pokemon-type.model';
import type { IVersionGroup } from '~/models/version-group.model';

export type IPokemonForm = {
	id: number;
	name: string;
	order: number;
	formOrder: number;
	isDefault: boolean;
	isBattleOnly: boolean;
	isMega: boolean;
	formName: string;
	types: [IPokemonType, IPokemonType] | [IPokemonType];
	versionGroup: IVersionGroup;
};
