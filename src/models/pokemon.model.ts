import type { IPokemonForm } from '~/models/pokemon-form.model';
import type { IPokemonSpecies } from '~/models/pokemon-species.model';
import type { IPokemonType } from '~/models/pokemon-type.model';

export type IPokemon = {
	id: number;
	name: string;
	height: number;
	isDefault: boolean;
	order: number;
	weight: number;
	forms: IPokemonForm[];
	species: IPokemonSpecies;
	types: [IPokemonType, IPokemonType] | [IPokemonType];
};
