import type { LocaleText } from '~/models/locale-text.model';

export type IPokedex = {
	id: number;
	name: string;
	isMainSeries: boolean;
	descriptions: LocaleText<'description'>[];
	names: LocaleText<'name'>[];
};
