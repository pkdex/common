import type { ILocation } from '~/models/location.model';
import type { LocaleText } from '~/models/locale-text.model';

export type IRegion = {
	id: number;
	name: string;
	names: LocaleText<'name'>[];
	locations: ILocation[];
};
