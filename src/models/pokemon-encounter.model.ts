import type { IEncounterMethod } from '~/models/encounter-method.model';
import type { IPokemon } from '~/models/pokemon.model';

export type IPokemonEncounter = {
	pokemon: IPokemon;
	method: IEncounterMethod;
	chance: number;
};
