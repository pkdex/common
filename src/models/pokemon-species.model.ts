// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { IPokedex } from '~/models/pokedex.model';

export type IPokemonSpecies = {
	id: number;
	name: string;
	order: number;
	isBaby: boolean;
	isLegendary: boolean;
	isMythical: boolean;

	/* eslint-disable no-tabs */
	// pokedexNumbers: {
	// 	number: number;
	// 	pokedex: IPokedex;
	// }[];
	/* eslint-enable no-tabs */
};
