import type { IPokemonEncounter } from '~/models/pokemon-encounter.model';
import type { LocaleText } from '~/models/locale-text.model';

export type ILocationArea = {
	id: number;
	name: string;
	names: LocaleText<'name'>[];
	pokemonEncounters: IPokemonEncounter[];
};
