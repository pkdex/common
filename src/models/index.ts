/**
 * Commonly-used models
 *
 * @public
 */

export type {
	Immutable,
	ImmutableArray,
	ImmutableObject
} from '~/models/immutable.model';
