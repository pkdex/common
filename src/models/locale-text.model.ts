export type LocaleText <T extends string> = {
	[K in T]: string;
} & {
	language: string;
};
