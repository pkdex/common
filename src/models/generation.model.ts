import type { LocaleText } from '~/models/locale-text.model';

export type IGeneration = {
	id: number;
	name: string;
	names: LocaleText<'name'>[];
};
