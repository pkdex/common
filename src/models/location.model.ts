import type { ILocationArea } from '~/models/location-area.model';
import type { LocaleText } from '~/models/locale-text.model';

export type ILocation = {
	id: number;
	name: string;
	names: LocaleText<'name'>[];
	areas: ILocationArea[];
};
