import type { IVersionGroup } from '~/models/version-group.model';
import type { LocaleText } from '~/models/locale-text.model';

export type IVersion = {
	id: number;
	name: string;
	names: LocaleText<'name'>[];
	versionGroup: IVersionGroup;
};
