<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [@pkdex/common](./common.md) &gt; [models](./common.models.md) &gt; [Immutable](./common.models.immutable.md)

## models.Immutable type

Describes a deeply-readonly type

**Signature:**

```typescript
export type Immutable<T> = T extends (infer R)[] ? ImmutableArray<R> : T extends Function ? T : T extends object ? ImmutableObject<T> : T;
```
**References:** [ImmutableArray](./common.models.immutablearray.md)<!-- -->, [ImmutableObject](./common.models.immutableobject.md)

